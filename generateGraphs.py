#!/usr/bin/env python3
################################################################################
# File:    main.py
# Version: 0.1
# Purpose: Main script called by the user for generating charts predicting the
#          spread of coronavirus in the future using a set of extrapolation
#          models. For more info, see: https://www.coviz.org
# Authors: Michael Goldenberg
# Authors: Michael Altfield <michael@michaelaltfield.net>
# Created: 2020-04-12
# Updated: 2020-04-12
################################################################################

################################################################################
#                                   IMPORTS                                    #
################################################################################

# IMPORTS

import os
import sys
import argparse
import logging

import readers
import models
import plotters

################################################################################
#                                  SETTINGS                                    #
################################################################################

# set of regions for which the graphs will be generated if nothing is specified
# by the user
DEFAULT_REGIONS = ['US']

# sets the default number of days into the future that the graph will show if
# nothing is specified by the user
DEFAULT_EXTRAPOLATION_DAYS = 30

################################################################################
#                                  FUNCTIONS                                   #
################################################################################

def main():

	#################
	# SETUP LOGGING #
	#################

	logging.basicConfig(stream=sys.stdout, level=logging.INFO)

	####################
	# HANDLE ARGUMENTS #
	####################

	# DEFINE COMMAND LINE ARGUMENTS

	# we use ArgmentParser to handle the user's command-line arguents
	parser = argparse.ArgumentParser(description='Generate charts predicting the spread of coronavirus in the future using a set of extrapolation models. For all regions, use --all-regions')

	# DATAPATH specifies the absolute path to our database file
	parser.add_argument(
	 '-d',
	 '--data-path',
	 dest = 'DATA_FILE_PATH',
	 action = 'store',
	 default =  os.path.join(
	  'data',
	  'jhcsse',
	  'csse_covid_19_data',
	  'csse_covid_19_time_series',
	  'time_series_covid19_confirmed_global.csv'
	 ),
	 help = 'specifies the path to the file that contains our COVID-10 data'
	)

	# REGIONS specifies a geographic location (except, eg, for Diamond Princess
	# Cruise Ship). This defaults just to the US to decrease processing time when
	# iterating.

	parser.add_argument(
	 '-r',
	 '--region',
	 dest = 'REGIONS',
	 action = 'append',
	 help = 'specifies a region to include in the output charts. Can be used multiple times. Values must match the region field used in the input data. For example, see the "Country_Region" field here https://github.com/CSSEGISandData/COVID-19/blob/master/csse_covid_19_data/UID_ISO_FIPS_LookUp_Table.csv'
	)

	# EARTH overrides REGION and generates the charts for all geographic
	# locations on earth
	parser.add_argument(
	 '-e',
	 '--earth',
	 dest = 'EARTH',
	 action='store_true',
	 default=False,
	 help = 'Used to generate graphs for all regions/countries on earth. Overrides --region'
	)

	# EXTRAPOLATION_DAYS specifies the number of days into the future to which the
	# graph should extrapolate

	parser.add_argument(
	 '-D',
	 '--extrapolation-days',
	 dest='EXTRAPOLATION_DAYS',
	 action = 'store',
	 type = int,
	 default = DEFAULT_EXTRAPOLATION_DAYS,
	 help='specifies the number of days into the future to which the graph should extrapolate its data. Defaults to 30 days.'
	)

	# PROCESS COMMAND LINE ARGUMENTS
	args = parser.parse_args()

	# CLEANUP COMMAND LINE ARGUMENTS

	# if no region is listed, set it to the default
	# note: we can't use 'default' in argparse with append or it will take on both
	#       the value of the default *and* the first option, not overriding the
	#       default as expected
	if not args.REGIONS:
		args.REGIONS = DEFAULT_REGIONS

	# remove duplicates from the list of regions
	args.REGIONS = list( set(args.REGIONS) )

	# warn the user that --earth overrides all --region args
	if args.EARTH and args.REGIONS != DEFAULT_REGIONS:
		logging.warning( "--earth cannot be combined with --region (ignoring --region)" )

	################################
	# PROCESS EXTRAPOLATION MODELS #
	################################

	# get our data from the data reader
	data = readers.jhcsse.read( args.DATA_FILE_PATH )

	# loop through each "subset" (normally a subset is a distinct
	# row/region/country) found in the data file
	for subset in data:

		# is the region for this iteration one that we want to produce a graph for?
		if not args.EARTH and subset['meta']['country'] not in args.REGIONS: 
			# the user didn't specify --earth and this region isn't in the REGIONS list
			# skip over this iteration
			continue

		logging.info( "processing region: " + str(subset['meta']['country']) )
		sys.stdout.write( "\t" )

		# the data reader knows the encoding of the given data file and already put
		# our timeseries in a special dict for us; get it.
		time_series = subset['time_series']

		# TODO: better automagic detection & looping through the available
		#       extrapolation models

		###############
		# e2a (3-DAY) #
		###############

		# "e2a (3-day)" is our example extrapolation model that does a second-degree
		# polynomial curve fit from the past 3-days of data using numpy's polyfit()
		# function

		# call the extrapolation model's make_extrapolate() function, which will
		# return a function that will output a y value for every x value we give it
		extrapolate = models.e2a_three.make_extrapolate(time_series)
		
		# now we're going to use the above-returned function to get the values on the
		# graph for hypothetical data in the future. start is one day after the last
		# day in our dataset
		start = len(time_series) - 1

		# stop is determined by the --extrapolation-days argument
		stop = start + args.EXTRAPOLATION_DAYS

		# prepare all the data for passing to plotly to draw the chart
		extrapolations = {
			'meta': models.e2a_three.meta(),
			'time_series': [extrapolate(x) for x in range(start, stop)]
		}

		# one dot denotes one model producing one graph
		sys.stdout.write( "." )
		plotters.plotly.plot(subset, extrapolations)

		###############
		# e2a (7-DAY) #
		###############

		# "e2a (7-day)" is our example extrapolation model that does a second-degree
		# polynomial curve fit from the past 3-days of data using numpy's polyfit()
		# function

		extrapolate = models.e2a_seven.make_extrapolate(time_series)
		
		start = len(time_series) - 1
		stop = start + args.EXTRAPOLATION_DAYS

		extrapolations = {
			'meta': models.e2a_seven.meta(),
			'time_series': [extrapolate(x) for x in range(start, stop)]
		}

		sys.stdout.write( "." )
		plotters.plotly.plot(subset, extrapolations)

		# delimate the output of this loop's iteration by printing a newline
		print( " " )

		################
		# my_new_model #
		################

		# "my new model" is our example extrapolation model for you to being to play
		# with. start editing the source of 'models/my_new_model.py' and--after
		# running this script--its output will show-up in:
		#  * 'output/my-new-models/<country>.hml'

		extrapolate = models.my_new_model.make_extrapolate(time_series)
		
		start = len(time_series) - 1
		stop = start + args.EXTRAPOLATION_DAYS

		extrapolations = {
			'meta': models.my_new_model.meta(),
			'time_series': [extrapolate(x) for x in range(start, stop)]
		}

		sys.stdout.write( "." )
		plotters.plotly.plot(subset, extrapolations)

		# delimate the output of this loop's iteration by printing a newline
		print( " " )

	print( "\nFinished! Check the 'output/' directory for the result. View them in your browser." )

if __name__ == '__main__':

	main()
