import os, re
import plotly.graph_objects as go

def plot(data, extrapolations):
	figure = go.Figure()

	title = '{title} ({province}, {region})'.format(
		title=extrapolations['meta']['title'],
		province=data['meta']['province'],
		region=data['meta']['region']
	)
	figure.update_layout(title=title)

	figure.add_trace(go.Scatter(
		x=[i for i in range(len(data['time_series']))],
		y=data['time_series']
	))

	start = len(data['time_series']) - 1
	stop = start + len(extrapolations['time_series'])
	figure.add_trace(go.Scatter(
		x=[i for i in range(start, stop)],
		y=extrapolations['time_series']
	))

	path = os.path.join('output', to_filename(extrapolations['meta']['title']))
	os.makedirs(path, exist_ok=True)

	if not data['meta']['province']:
		output_filename = to_filename('{region}.html'.format(
			region=data['meta']['region'], 
			province=data['meta']['province']
		))
	else:
		output_filename = to_filename('{region}-{province}.html'.format(
			region=data['meta']['region'], 
			province=data['meta']['province']
		))

	figure.write_html(os.path.join(path, output_filename ))

def to_filename(s):
	return re.sub('\s+', '-', s).lower()
