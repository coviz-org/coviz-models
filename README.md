# models

Models for extrapolating COVID-19 data.

## How to use

This repository was designed to be as simple & fast as possible for contributors to begin writing and testing extrapolation models for the spread of coronavirus from the COVID-19 dataset provided.

> This guide was tested on Debian 10. If you have managed to get this to work in other distributions, please open an issue documenting the steps/commands for your OS/distro, and we'll add it the wiki

### Bootstrap

To begin, run the following commands

```
sudo apt-get -y install git python3-pip

# use --recurse-submodules so our <1M submodule repo with the CSV dataset is
# also fetched: https://gitlab.com/coviz-org/data-jhcsse
git clone --recurse-submodules git@gitlab.com:coviz-org/coviz-models.git

cd coviz-models
pip3 install requirements.txt
```

### Generate graphs

This repo includes two example extraplation models. Both are a very simple second-degree polynomial curve-fit based on previous data. One curve fits to the past 3 days of data. one curve fits to the past 7 days of data.

Run the following commands from the `coviz-models` dir to generate the graphs using these models

```
./generateGraphs.py
ls
```

You should now see a directory named `output`. Inside of that directory you'll see three more directories--one for each of the different extrapolation models:

```
user@coviz:~/sandbox/coviz-models$ ls output/
my-new-model                          projections-based-on-last-three-days
projections-based-on-last-seven-days
user@coviz:~/sandbox/coviz-models$ 
```

Inside of each of those models' directories, you'll an html file for each of the dataset's regions (ie: Afghanistan, US, Diamond Princess, etc).

```
user@coviz:~/sandbox/coviz-models$ ls output/projections-based-on-last-three-days/
us.html
user@coviz:~/sandbox/coviz-models$ 
```

By default, only the US graphs are created. If you'd like to generate graphs for additional regions, specify one or more regions with `--region`. If you'd like to generate the graphs for all the regions, use `--earth`

> Note: Using `--earth` will take a long time!

```
# generate files for graphs of both Afghanistan and the United States
./generateGraphs.py --region Afghanistan --region US

# generate files for graphs of all regions on earth
./generateGraphs.py --earth
```

### Viewing Graphs

Open the html files in your browser to see the graphs.

```
firefox output/projections-based-on-last-three-days/us.html
```

### Creating your own model

If you'd like to create your own extraplation model, you can edit the `models/my_new_model.py` file to your liking.

You just need its `make_extrapolate` function to return a function that will take a single argument (the `x` value on the graph) and return another number (that `x`'s cooresponding y value on the graph).

TODO: Link to blog post when published

## Updating

To update the COVID-10 dataset, run this comand to pull our `data-*` submodules to match the latest on the remote (gitlab.com)

```
git submodule foreach git pull origin master
```

And, of course, to keep the rest of this repo's code up-to-date, run a `git pull`

```
git pull
```
