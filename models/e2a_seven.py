import numpy

def make_extrapolate(data):
	x = [i for i in range(len(data))]
	y = data

	# fit exponential curve to last seven days of data 
	curve = numpy.polyfit(x[-7:], y[-7:], 2)

	# create function to be applied for extrapolation
	extrapolate = numpy.poly1d(curve)

	return extrapolate

def meta():
	return {
		'title': 'Projections Based on Last Seven Days'
	}
