import csv
import datetime

def read(path):
    with open(path, newline='') as f:
        data = []
        rows = [row for row in csv.reader(f)]
        start = parse_date(rows[0][4])
        for row in rows[1:]:
            data.append({
                'meta': {
                    'province': row[0],
                    'state': row[0],
                    'country': row[1],
                    'region': row[1],
                    'coordinates': {
                        'lat': float(row[2]),
                        'long': float(row[3])
                    },
                    'start': start,
                },
                'time_series': list(map(int, row[4:]))
            })
        return data


def parse_date(s):
    mdy = list(map(int, s.split('/'))) # ugh
    return datetime.date(2000 + mdy[2], mdy[0], mdy[1]) # uggggh

